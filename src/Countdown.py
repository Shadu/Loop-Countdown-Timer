import time
from playsound import playsound


alarmSound = "alarm-sound.mp3"


def calculateSeconds(hours, mins, seconds):
    totalSeconds = (int(hours) * 60) + (int(mins) * 60) + int(seconds)
    global countDown
    countDown = totalSeconds


def startCountdown():
    global startTime
    startTime = time.time()


def playAlarmSound():
    playsound(alarmSound)


def checkLoop():
    if loop == True:
        startCountdown()
    else:
        return True


def checkCountdown():
    timePassed = time.time() - startTime
    if timePassed >= countDown:
        print("Countdown Reached")
        playAlarmSound()
        if checkLoop() == True:
            return True
    return False


def userSetTimer():
    hours = input("Insert how many hours. ")
    mins = input("Insert how many minutes. ")
    seconds = input("Insert how many seconds. ")
    calculateSeconds(hours, mins, seconds)


def loopSet():
    global loop
    loopInput = input("Loop the countdown, yes or no?: ")
    if loopInput == "yes":
        loop = True
    elif loopInput == "no":
        loop = False


def main():
    userSetTimer()
    loopSet()
    startCountdown()
    while checkCountdown() == False:
        time.sleep(1)


main()
